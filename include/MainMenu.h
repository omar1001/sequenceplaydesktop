#ifndef MAINMENU_H
#define MAINMENU_H

#include "Button.h"


class MainMenu
{
    public:
        float playBtnWidth;
        float fpsBtnWidth;
        float btnAnimTime;
        int fps_selected;
        int state;

        Button hardBtn;
        Button easyBtn;
        Button fps60Btn;
        Button fps30Btn;


        MainMenu();
        void init();
        void update(float delta);
        void display();


        void changeFramePerSecond(int option);
        void play(int difficulty);

    protected:
    private:
};

#endif // MAINMENU_H
