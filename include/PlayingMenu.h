#ifndef PLAYINGMENU_H
#define PLAYINGMENU_H
#include <vector>
#include <Circle.h>
#include <Vertix.h>

class PlayingMenu
{
    public:
        std::vector <Circle*> *circles;
        PlayingMenu();

        void init(std::vector <Circle*> *circles);
        void update(float delta);
        void display();
        void onRefresh();
        bool mouseHandler(int button, int state, int x, int y);
        bool mouseInside(Vertix v);

    protected:

    private:
};

#endif // PLAYINGMENU_H
