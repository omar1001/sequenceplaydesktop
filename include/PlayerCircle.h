#ifndef PLAYERCIRCLE_H
#define PLAYERCIRCLE_H
#include "Circle.h"
#include "Vertix.h"

class PlayerCircle : public Circle
{
    public:
        int state;
        Vertix mouseCurrent;
        Vertix aimingEndPoint;
        Vertix aimingStartPoint;
        Vertix startPosition;
        float shootingStateTime;

        PlayerCircle();
        void shootingState();
        void aimingState();
        void init(float rad, Vertix pos, int num);
        void update(float delta);
        virtual void display();
        void changeAmingEndPoint(Vertix v);
        void replaceCircle(Circle* c);
        void updateAimingStartPoint();


    protected:

    private:
};

#endif // PLAYERCIRCLE_H
