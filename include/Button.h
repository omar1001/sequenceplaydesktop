#ifndef BUTTON_H
#define BUTTON_H


class Button
{
    public:
        float start_x;
        float start_y;
        float final_x;
        float final_y;
        float width;

        Button();
        Button(float width, float start_x, float start_y);

    protected:

    private:
};

#endif // BUTTON_H
