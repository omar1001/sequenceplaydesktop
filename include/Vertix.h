#ifndef VERTIX_H
#define VERTIX_H


class Vertix
{
    public:
        Vertix();
        Vertix(float x, float y);
        float x;
        float y;
    protected:

    private:
};

#endif // VERTIX_H
