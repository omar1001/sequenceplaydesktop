#ifndef NORMALCIRCLE_H
#define NORMALCIRCLE_H
#include "Circle.h"
#include "Vertix.h"

class NormalCircle: public Circle
{
    public:
        NormalCircle();
        void init(float rad, Vertix ver, int num);
        void update(float delta);
        void display();

    protected:

    private:
};

#endif // NORMALCIRCLE_H
