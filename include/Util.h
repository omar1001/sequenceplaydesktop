#ifndef UTIL_H
#define UTIL_H
#include <iostream>
#include <vector>
#include "Circle.h"
#include "Vertix.h"

class Util
{
    public:
        Util();

        static bool checkCircleValid(Circle* circle, std::vector<Circle*> circles, int start);
        static std::vector<Circle*> generateCircles(int n);
        static float distance(Vertix x, Vertix y);
        static Vertix toLogicalCoordinates(Vertix v);

    protected:

    private:
};

#endif // UTIL_H
