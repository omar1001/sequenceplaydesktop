#ifndef CIRCLE_H
#define CIRCLE_H
#include "Vertix.h"

#include <string>

class Circle
{
    public:
        float radius;
        Vertix position;
        int type = 0;
        int num;
        Circle();
        void init(float radius, Vertix position, int num);
        virtual void display();
        virtual void update(float delta);

        static void displayCircle(Vertix point, float radius);
        static void displayNumber(Vertix point, int num);


    protected:

    private:
};

#endif // CIRCLE_H
