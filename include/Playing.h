#ifndef PLAYING_H
#define PLAYING_H
#include <vector>
#include "Circle.h"
#include "PlayerCircle.h"
#include "PlayingMenu.h"
using namespace std;

class Playing
{
    public:
        vector <Circle*> circles;
        PlayerCircle* playerCircle;
        int circlesCount = 0;
        PlayingMenu* playingMenu;

        Playing();
        void init (int dfclty);
        void update (float delta);
        void display ();
        void closePlaying();
        void dispose();

    protected:

    private:
        short difficulty;
};

#endif // PLAYING_H
