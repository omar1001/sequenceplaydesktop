#include <GL/glut.h>
#include <stdlib.h>
#include <iostream>
#include "Constants.h"
#include "MainMenu.h"
#include "Playing.h"

float lastTime = 0;
int fps = FRAME_PER_SECOND_60;
int difficulty = DIFFICULTY_HARD;
short state = STATE_PLAYING;

Playing playing;
MainMenu mainMenu;

void MainMenu::changeFramePerSecond(int option) {
    fps = option;
}
void MainMenu::play(int dfclty) {
    difficulty = dfclty;
    state = STATE_PLAYING;
    playing.init(dfclty);
}
void Playing::closePlaying() {
    //state = STATE_MAIN_MENU;
    this -> dispose();
    this -> init(difficulty);
}

void init (float r,float g,float b) {
    glClearColor(r, g, b, 0);
    glMatrixMode(GL_PROJECTION);
    gluOrtho2D(0, WIDTH, HEIGHT, 0);

    // TODO: call init in the main objects
    //mainMenu.init();
    //mainMenu.init();
    playing.init(difficulty);
}

void update (int val) {
    // Creating delta time to pass to the other objects.
    float currentTime = (glutGet(GLUT_ELAPSED_TIME));
    float delta = (currentTime - lastTime) / 1000;
    lastTime = currentTime;

    // TODO: call update method in the main objects
    switch (state) {
        case STATE_MAIN_MENU:
            mainMenu.update(delta);
            break;
        case STATE_PLAYING:
            playing.update(delta);
            break;
    }

    glutPostRedisplay();
}


void display ()
{
    glClear(GL_COLOR_BUFFER_BIT);

    // TODO: call display method in the main objects.
    switch (state) {
        case STATE_MAIN_MENU:
            mainMenu.display();
            break;
        case STATE_PLAYING:
            playing.display();
            break;
    }

    glFlush();
    glutSwapBuffers();

    // set timer to the next frame update
    glutTimerFunc(fps, update, 0);
}

int main (int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH | GLUT_MULTISAMPLE);
    glEnable(GL_MULTISAMPLE);

    //glutInitWindowSize(REAL_WIDTH, REAL_HEIGHT);
    //glutInitWindowPosition(500, 500);
    glutCreateWindow("Sequence Play");
    init(245, 245, 245);
    glutDisplayFunc(display);
    glutFullScreen();

    glutMainLoop();

}






