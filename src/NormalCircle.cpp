#include "NormalCircle.h"
#include "Circle.h"

#include <GL/glut.h>
#include <iostream>


NormalCircle::NormalCircle()
{
    //ctor
}

void NormalCircle::init(float rad, Vertix pos, int num) {
    Circle::init(rad, pos, num);

    //TODO: do the normal circle initialization if exist
}

void NormalCircle::update(float delta) {
    Circle::update(delta);

}


void NormalCircle::display() {
    glColor3f(0, 0, 0);
    Circle::displayCircle(position, radius);
    Circle::displayNumber(position, num);
}
