#include "Util.h"
#include "Circle.h"
#include "Constants.h"
#include "PlayerCircle.h"
#include "NormalCircle.h"
#include "Vertix.h"

#include <time.h>
#include <cstdlib>
#include <vector>
#include <math.h>
#include <GL/glut.h>


Util::Util()
{
    //ctor
}

//not my function.
float FindDistanceToSegment(float x1, float y1, float x2, float y2, float pointX, float pointY)
{
    float diffX = x2 - x1;
    float diffY = y2 - y1;
    if ((diffX == 0) && (diffY == 0)) {
        diffX = pointX - x1;
        diffY = pointY - y1;
        return sqrt(diffX * diffX + diffY * diffY);
    }
    float t = ((pointX - x1) * diffX + (pointY - y1) * diffY) / (diffX * diffX + diffY * diffY);
    if (t < 0) {
        diffX = pointX - x1;
        diffY = pointY - y1;
    } else if (t > 1) {
        diffX = pointX - x2;
        diffY = pointY - y2;
    } else{
        diffX = pointX - (x1 + t * diffX);
        diffY = pointY - (y1 + t * diffY);
    }
    return sqrt(diffX * diffX + diffY * diffY);
}

float Util::distance(Vertix a, Vertix b) {
    return sqrt(pow(a.y - b.y, 2) + pow(a.x - b.x, 2));
}

bool isCirclesOverlapping(Circle* a, Circle* b) {
    return Util::distance(a -> position, b -> position) <= (a -> radius + b -> radius);
}

bool isBetweenCircles(Circle* circle, Circle* a, Circle* b) {
    // check if the circle is not between a & b.
    if (Util::distance(circle -> position, a -> position) > Util::distance(a -> position, b -> position)) return false;
    if (Util::distance(circle -> position, b -> position) > Util::distance(a ->position, b -> position)) return false;

    //check if not inside the range between them using the perpendicular distance
    // between the circle center and the line between a & b centers.
    float perDist = FindDistanceToSegment(a -> position.x, a -> position.y, b -> position.x, b -> position.y, circle -> position.x, circle -> position.y);
    return perDist < (std::max(a -> radius, b -> radius) + circle -> radius);

}

bool Util::checkCircleValid(Circle* circle, std::vector <Circle*> circles, int start) {
    int n = circles.size();
    for (int i=start; i<n; i++) {
        if (isCirclesOverlapping(circle, circles[i])) {
            return false;
        }
    }
    for (int i=start; i < n-1; i++) {
        if (isBetweenCircles(circle, circles[i], circles[i+1])) {
            return false;
        }
    }
    return true;
}

Vertix generateVertix() {
    const int xx = (PLAYING_FIELD_RANGE_X2 - PLAYING_FIELD_RANGE_X1) * 10;
    const int yy = (PLAYING_FILED_RANGE_Y2 - PLAYING_FIELD_RANGE_Y1) * 10;
    Vertix vertix((rand() % xx) / 10.0 + PLAYING_FIELD_RANGE_X1, (rand() % yy) / 10.0 + PLAYING_FIELD_RANGE_Y1);
    return vertix;
}
int generateNum(int lvl) {
    return (rand() % CIRCLE_NUM_LVL_INCREMENTER) + lvl;
}

std::vector<Circle*> Util::generateCircles(int n) {
    std::cout << "generating circles..\n";
    std::vector<Circle*> circles;
    circles.clear();
    circles.resize(n);

    // player circle first circle
    int circlesNumLvl = 0;
    srand(time(NULL));
    Vertix ver = generateVertix();
    PlayerCircle* player = new PlayerCircle();
    player -> type = CIRCLE_PLAYER_TYPE;
    int num = generateNum(circlesNumLvl);
    player -> init(CIRCLE_PLAYER_RADIUS, ver, num);
    circles[n-1] = player;

    int err = (-1 * n) + 1;

    for (int i=n-2; i > -1; i--) {
        circlesNumLvl += CIRCLE_NUM_LVL_INCREMENTER;
        num = generateNum(circlesNumLvl);
        NormalCircle* circle = new NormalCircle();
        circle -> type = 0;
        do {

            err++;

            ver = generateVertix();
            circle -> init(CIRCLE_NORMAL_RADIUS, ver, num);
        } while (!checkCircleValid(circle, circles, i+1));
        circles[i] = circle;
        std::cout << "circle: " << i << " generated.\n";
    }

    std:: cout << "circles corrected: " << err << "\n";

    return circles;

}

Vertix Util::toLogicalCoordinates(Vertix v) {
    Vertix newV((v.x*WIDTH)/glutGet(GLUT_WINDOW_WIDTH), (v.y*HEIGHT)/glutGet(GLUT_WINDOW_HEIGHT));
    return newV;
}

















