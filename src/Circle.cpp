#include "Constants.h"
#include "Circle.h"
#include "Vertix.h"

#include <GL/glut.h>
#include <math.h>
#include <vector>
#include <iostream>

Circle::Circle()
{

}

void Circle::displayCircle(Vertix point, float radius) {
    // implement cirlce displaying.

    glBegin(GL_LINE_LOOP);
    for (int ii = 0; ii < CIRCLE_DRAWING_SEGMENTS; ii++)   {
        float theta = 2.0f * 3.1415926f * float(ii) / float(CIRCLE_DRAWING_SEGMENTS);//get the current angle
        float x = radius * cosf(theta);//calculate the x component
        float y = radius * sinf(theta);//calculate the y component
        glVertex2f(x + point.x, y + point.y);//output vertex
    }
    glEnd();
}
void Circle::displayNumber(Vertix point, int num) {

    std::vector <char> str;
    do {
        char c = (num % 10) + '0';
        str.push_back(c);
        num /= 10;
    } while (num != 0);
    glRasterPos2d(point.x - str.size()/CHAR_SIZE_DIVIDER, point.y + 0.5);

    for (int i=str.size()-1; i > -1; i--) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, str[i]);
    }
    glEnd();
}

void Circle::init(float radius, Vertix position, int num) {
    this -> radius = radius;
    this -> position = position;
    this -> num = num;

}

void Circle::update(float delta) {

}


void Circle::display() {

}

