#include <GL/glut.h>
#include <iostream>
#include <vector>
#include <typeinfo>

#include "Playing.h"
#include "Constants.h"
#include "Vertix.h"
#include "Util.h"
#include "Circle.h"
#include "PlayerCircle.h"

Playing::Playing()
{
    //ctor
}

PlayerCircle* _playerCircle;
PlayingMenu* _playingMenu;
Playing* _playing;
void mouseHandler(int button, int state, int x, int y) {
    if (_playingMenu -> mouseHandler(button, state, x, y)) return;
    if (_playerCircle -> state == PLAYER_CIRCLE_AIMING_STATE && button == GLUT_LEFT_BUTTON && state == GLUT_UP) {
        _playerCircle -> changeAmingEndPoint(Util::toLogicalCoordinates(Vertix(x, y)));
        _playerCircle -> shootingState();
    }
}
void mouseMotionHandler(int x, int y) {
    if (_playingMenu -> mouseInside(Vertix(x, y))) return;
    if (_playerCircle -> state == PLAYER_CIRCLE_AIMING_STATE) {
        _playerCircle -> changeAmingEndPoint(Util::toLogicalCoordinates(Vertix(x, y)));
    }
}

void Playing::init(int dfclty) {
    difficulty = dfclty;
    circlesCount = dfclty / CIRCLE_NUM_LVL_INCREMENTER;
    circles = Util::generateCircles(circlesCount);
    playerCircle = (PlayerCircle*)circles[circles.size()-1];
    _playerCircle = playerCircle;
    glutMouseFunc(mouseHandler);
    glutPassiveMotionFunc(mouseMotionHandler);
    for (int i=0; i<circles.size(); i++) std:: cout << circles[i] -> num << ", ";
    std::cout << "\n";
    this -> playingMenu = new PlayingMenu();
    this -> playingMenu -> init(&circles);
    _playingMenu = playingMenu;
    _playing = this;
}

void Playing::update(float delta) {
    for (int i=0; i<circles.size(); i++) {
        circles[i] -> update(delta);
    }
    if (playerCircle -> state == PLAYER_CIRCLE_SHOOTING_STATE) {
        if (Util::distance(playerCircle ->position, Vertix(WIDTH / 2, HEIGHT / 2)) > max(WIDTH, HEIGHT)) {
            this -> closePlaying();
        }
        for (int i=0; i<circles.size(); i++) {
        //std::cout << "size: " << circles.size() << ", i: " << i << "\n";
            if (circles[i] -> type == CIRCLE_PLAYER_TYPE) continue;
            //check if a player circle collided with another circle
            if (Util::distance(playerCircle -> position, circles[i] -> position) < playerCircle -> radius + circles[i] -> radius) {
                playerCircle -> replaceCircle(circles[i]);
                delete circles[i];
                circles[i] = NULL;
                circles[i] = playerCircle;
                for (int ii=0; ii < circles.size(); ii++) {
                    if (circles[ii] -> type == CIRCLE_PLAYER_TYPE) continue;
                    if (circles[ii] -> num < playerCircle -> num) {
                        circles.pop_back();
                        return this -> closePlaying();
                    }
                }
                break;
            }
        }
        int occ = 0;
        for (int i=0; i<circles.size(); i++) {
            if (circles[i] -> type == CIRCLE_PLAYER_TYPE) {
                occ++;
                if (occ == 1) continue;
                for (int ii=i; ii<circles.size()-1; ii++) {
                    circles[ii] = circles[ii+1];
                }
                circles.pop_back();
            }
        }
    }
    playingMenu -> update(delta);
}

void Playing::display() {
    for (int i=0; i<circles.size(); i++) {
        circles[i] -> display();
    }

    playingMenu -> display();
}
void PlayingMenu::onRefresh() {
    _playing -> closePlaying();
}

void Playing::dispose() {

    for (int i=0; i<circles.size(); i++) {
        if (circles[i] == NULL) continue;

        delete circles[i];
        circles[i] = NULL;
    }
    delete playingMenu;
}
















