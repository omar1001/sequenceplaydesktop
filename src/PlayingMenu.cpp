#include "PlayingMenu.h"
#include <vector>
#include <GL/glut.h>
#include "Circle.h"
#include "Constants.h"
#include "Vertix.h"
#include "Util.h"

PlayingMenu::PlayingMenu()
{
    //ctor
}

int refreshWidth = 0;
int refreshButtonState;
float refreshAnimCount;
void calcRefreshWidth() {
    for (int i=0; REFRESH[i] != '\0'; i++) {
        refreshWidth += glutBitmapWidth(GLUT_BITMAP_HELVETICA_18, 'R');
    }
}

bool PlayingMenu::mouseHandler(int button, int state, int x, int y) {
    Vertix v = Util::toLogicalCoordinates(Vertix(x, y));
    if (v.x < refreshWidth + PLAYING_TOP_MENU_HEIGHT / 2 && v.y < PLAYING_TOP_MENU_HEIGHT) {
        refreshButtonState = PLAYING_MENU_REFRESH_BUTTON_CLICK_STATE;
        return true;
    }
    return false;
}
bool PlayingMenu::mouseInside(Vertix v) {
    return Util::toLogicalCoordinates(v).y <= PLAYING_TOP_MENU_HEIGHT;
}


void PlayingMenu::init(std::vector <Circle*> *circles) {
    this -> circles = circles;

    calcRefreshWidth();
    refreshButtonState = PLAYING_MENU_REFRESH_BUTTON_STEADY_STATE;
    refreshAnimCount = 0;
}



void PlayingMenu::update(float delta) {
    switch (refreshButtonState) {
        case PLAYING_MENU_REFRESH_BUTTON_CLICK_STATE:
            refreshAnimCount += delta;
            if (refreshAnimCount >= PLAYING_MENU_REFRESH_BUTTON_ANIM_LIMIT) {
                this -> onRefresh();
            }
            break;
        case PLAYING_MENU_REFRESH_BUTTON_STEADY_STATE:

            break;
    }

}


void PlayingMenu::display() {
    // menu bar
    glColor3f(0, 0, 0);
    glBegin(GL_POLYGON);
    glVertex2d(0, PLAYING_TOP_MENU_HEIGHT);
    glVertex2d(WIDTH, PLAYING_TOP_MENU_HEIGHT);
    glVertex2d(WIDTH, 0);
    glVertex2d(0, 0);
    glEnd();

    // refresh button.
    switch (refreshButtonState) {
        case PLAYING_MENU_REFRESH_BUTTON_CLICK_STATE:
            glColor3f(255, 0, 0);
            break;
        case PLAYING_MENU_REFRESH_BUTTON_STEADY_STATE:
            glColor3f(255, 255, 255);
            break;
    }

    glRasterPos2d(PLAYING_TOP_MENU_HEIGHT / 2, PLAYING_TOP_MENU_HEIGHT / 1.5);
    for (int i=0; REFRESH[i] != '\0'; i++) {
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, REFRESH[i]);
    }
    glEnd();

    // numbers
    glColor3f(100, 100, 100);
    for (int i=0; i<circles->size() - 1; i++) {
        if (i == circles -> size() - 2) {
            glColor3f(255, 0, 0);
        }
        Circle::displayNumber(Vertix(WIDTH - (PLAYING_TOP_MENU_HEIGHT / 2) * i - 5, PLAYING_TOP_MENU_HEIGHT / 2), circles->at(i)->num);
    }
}






















