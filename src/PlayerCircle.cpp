#include <iostream>
#include <GL/glut.h>
#include <math.h>

#include "PlayerCircle.h"
#include "Circle.h"
#include "Vertix.h"
#include "Constants.h"
#include "Util.h"

PlayerCircle::PlayerCircle()
{
    //ctor
}

void PlayerCircle::aimingState() {
    this -> state = PLAYER_CIRCLE_AIMING_STATE;
}
void PlayerCircle::shootingState() {
    this -> state = PLAYER_CIRCLE_SHOOTING_STATE;
    this -> shootingStateTime = 0;
    this -> startPosition = this -> position;
}

void PlayerCircle::init(float rad, Vertix pos, int num) {
    Circle::init(rad, pos, num);
    this -> state = PLAYER_CIRCLE_AIMING_STATE;
    this -> shootingStateTime = 0;
    this -> startPosition = pos;
    this -> updateAimingStartPoint();
    //TODO: do the player circle initialization if exist
}

void PlayerCircle::changeAmingEndPoint(Vertix v) {
    this -> aimingEndPoint = v;
    this -> updateAimingStartPoint();
}
void PlayerCircle::updateAimingStartPoint() {
    float t = this -> radius / Util::distance(this -> position, this -> aimingEndPoint);
    aimingStartPoint.x = (1.0 - t) * this -> position.x + t * aimingEndPoint.x;
    aimingStartPoint.y = (1.0 - t) * this -> position.y + t * aimingEndPoint.y;
}
void PlayerCircle::replaceCircle(Circle* c) {
    this -> position = c -> position;
    this -> num = c -> num;
    this -> aimingState();
    this -> updateAimingStartPoint();
}

void PlayerCircle::update(float delta) {
    switch (state) {
        case PLAYER_CIRCLE_AIMING_STATE:

            break;
        case PLAYER_CIRCLE_SHOOTING_STATE:
            shootingStateTime += delta;
            //TODO: update current position.
            position.y = (shootingStateTime / PLAYER_CIRCLE_REACH_TIME * (aimingEndPoint.y - startPosition.y)) + startPosition.y;
            position.x = (shootingStateTime / PLAYER_CIRCLE_REACH_TIME * (aimingEndPoint.x - startPosition.x)) + startPosition.x;

            break;
    }
}

void PlayerCircle::display() {
    glColor3f(255, 0, 0);
    Circle::displayCircle(position, radius);
    Circle::displayNumber(position, num);
    switch (state) {
        case PLAYER_CIRCLE_AIMING_STATE:
            //TODO: display the line of aiming.
            glBegin(GL_LINES);
            glVertex2f(aimingStartPoint.x, aimingStartPoint.y);
            glVertex2f(aimingEndPoint.x, aimingEndPoint.y);
            glEnd();
            break;
        case PLAYER_CIRCLE_SHOOTING_STATE:



            break;
    }


}










